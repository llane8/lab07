﻿// Lab 07 - GAME 221 - JCCC - Prof. T. Fisher
// This script is a simple player controller that syncs in a multiplayer game
// This script is placed on a game object that is the player character
//@author Linda Lane - taken from lab by T. Fisher
//@date Oct. 26, 2016

using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class Lab02a_PlayerControl : NetworkBehaviour {

    // Set up the PlayerState struct to hold all the variables of the players position and rotation
    struct PlayerState
    {
        public float posX, posY, posZ;
        public float rotX, rotY, rotZ;
    }

    // Make this state a SyncVar so it is updated to server and all clients every time it changes
    [SyncVar]
    PlayerState state;

    // Use this for initialization
    void Start()
    {
        // call the InitState  and SyncState to tell server and everyone else what these are
        InitState();
        SyncState();
    }

    // Update is called once per frame
    void Update()
    {
        if(isLocalPlayer)
        {
            KeyCode[] possibleKeys = { KeyCode.A, KeyCode.S, KeyCode.D, KeyCode.W, KeyCode.Q, KeyCode.E, KeyCode.Space};
            foreach (KeyCode possibleKey in possibleKeys)
            {
                if (!Input.GetKey(possibleKey)) //If the currently observed key code is not pressed
                    continue;                   //  then do nothing.

                CmdMoveOnServer(possibleKey);
            }
        }
        SyncState();
    }

    // Update the player's state based on a key input
    [Command]
    void CmdMoveOnServer(KeyCode pressedKey)
    {
        // Vector3 fwd = transform.TransformDirection(Vector3.forward);

        state = Move(state, pressedKey);
    }

    // InitState() sets up the default values of the player state
    // Must have the [Server] attribute (only allowed to be called on the Server)
    [Server]
    void InitState()
    {
        state = new PlayerState
        {
            posX = -119f,
            posY = 165.08f,
            posZ = -924f,
            rotX = 0f,
            rotY = 0f,
            rotZ = 0f
        };
    }

    // SyncState( ) sync's the player's model to the values stored inside the syncvar PlayerState 
    // so player has valid position and rotation
    void SyncState()
    {
        transform.position = new Vector3(state.posX, state.posY, state.posZ);
        transform.rotation = Quaternion.Euler(state.rotX, state.rotY, state.rotZ);
    }

    // Move(args) method will move the player based on it's previous state and the keyboard input
    // then it updates all clients
    // It returns the PlayerState 
    PlayerState Move(PlayerState previous, KeyCode newKey)
    {
        float deltaX = 0, deltaY = 0, deltaZ = 0;
        float deltaRotationY = 0;

        // Assume forward = +z, backward = -z, right = +x, left = -x, rotation left = -y, rotation right = +y
        switch (newKey)
        {
            case KeyCode.Q:  // left
                deltaX = -0.5f;
                break;
            case KeyCode.S:   // backward
                deltaZ = -0.5f;
                break;
            case KeyCode.E:     // right
                deltaX = 0.5f;
                break;
            case KeyCode.W:     // forward
                deltaZ = 0.5f;
                break;
            case KeyCode.A:     // rotate left
                deltaRotationY = -1f;
                break;
            case KeyCode.D:     // rotate right
                deltaRotationY = 1f;
                break;
          
        }

        return new PlayerState
        {
            posX = deltaX + previous.posX,
            posY = deltaY + previous.posY,
            posZ = deltaZ + previous.posZ,
            rotX = previous.rotX,
            rotY = deltaRotationY + previous.rotY,
            rotZ = previous.rotZ
        };
    }
}
