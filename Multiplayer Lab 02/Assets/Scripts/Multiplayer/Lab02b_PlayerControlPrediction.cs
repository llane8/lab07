﻿// Lab 07 - GAME 221 - JCCC - Prof. T. Fisher
// This script is a simple player controller that syncs in a multiplayer game
// This script is placed on a game object that is the player character
//@author Linda Lane - taken from lab by T. Fisher
//@date Oct. 26, 2016

using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using System.Collections.Generic;

public enum CharacterState
{
    Idle = 0,
    WalkingForward = 1,
    WalkingBackwards = 2,
    RunningForward = 5,
    RunningBackwards = 6,
    Jumping = 4
}

public class Lab02b_PlayerControlPrediction : NetworkBehaviour
{
    [Tooltip("Speed of the player walking animation")]
    public float walkSpeed;

    [Tooltip("Speed of the player running animation")]
    public float runSpeed;

    // Set up the PlayerState struct to hold all the variables of the players position and rotation
    struct PlayerState
    {
        public int movementNumber;
        public float posX, posY, posZ;
        public float rotX, rotY, rotZ;
        public CharacterState animationState;
        public float walkSpeed;
        public float runSpeed;
        public bool isRunning;
        public bool isJumping;

    }

    // Make this state a SyncVar so it is updated to server and all clients every time it changes
    [SyncVar(hook = "OnServerStateChanged")]
    PlayerState serverstate;  // This will represent the state of the player on the SERVER so it needs to be syncvar

    PlayerState predictedState; // This will represent the state of the player as predicted on CLIENT only

    // The Queue will represent the moves that the player is trying to perform
    // that have not been acknowledged by the server yet
    // The Queue is a FIFO list
    Queue<KeyCode> pendingMoves;

    // Store the current animation state
    CharacterState characterAnimationState;

    // create animator controller value
    public Animator animController;

    public bool IsGrounded { get; private set; }

    // Use this for initialization
    void Start()
    {
        // call the InitState  and SyncState to tell server and everyone else what these are
        InitState();
        predictedState = serverstate;
        if(isLocalPlayer)
        {
            pendingMoves = new Queue<KeyCode>();
            UpdatePredictedState();
        }
        SyncState();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (isLocalPlayer)
        {
            Debug.Log("Pending moves: " + pendingMoves.Count);

            KeyCode[] possibleKeys = { KeyCode.A, KeyCode.S, KeyCode.D, KeyCode.W, KeyCode.Q, KeyCode.E, KeyCode.Space, KeyCode.LeftShift };

            bool somethingPressed = false;

            foreach (KeyCode possibleKey in possibleKeys)
            {
                if (!Input.GetKey(possibleKey)) //If the currently observed key code is not pressed
                    continue;                   //  then do nothing.

                somethingPressed = true;
                pendingMoves.Enqueue(possibleKey);
                Debug.Log("local? " + isLocalPlayer + " pending? " + pendingMoves.Count);
                UpdatePredictedState();
                CmdMoveOnServer(possibleKey);
            }

            if(!somethingPressed)
            {
                pendingMoves.Enqueue(KeyCode.Alpha0);
                UpdatePredictedState();
                CmdMoveOnServer(KeyCode.Alpha0);
            }
        }
        SyncState();
    }
   
    // Update the player's state based on a key input
    [Command]
    void CmdMoveOnServer(KeyCode pressedKey)
    {
        serverstate = Move(serverstate, pressedKey);
    }

    // InitState() sets up the default values of the player state
    [Server]
    void InitState()
    {
        serverstate = new PlayerState
        {
            movementNumber = 0,
            posX = -119f,
            posY = 165.08f,
            posZ = -924f,
            rotX = 0f,
            rotY = 0f,
            rotZ = 0f,
            isRunning = false,
            isJumping = false

        };
    }

    // SyncState( ) sync's the player's model to the values stored inside the syncvar PlayerState
    void SyncState()
    {
        PlayerState stateToRender = isLocalPlayer ? predictedState : serverstate;
        transform.position = new Vector3(stateToRender.posX, stateToRender.posY, stateToRender.posZ);
        transform.rotation = Quaternion.Euler(stateToRender.rotX, stateToRender.rotY, stateToRender.rotZ);
        animController.SetInteger("CharacterState", (int)stateToRender.animationState);
        if ((int)stateToRender.animationState == 1 | (int)stateToRender.animationState == 2)
            animController.SetFloat("WalkSpeed", walkSpeed);
        if ((int)stateToRender.animationState == 5 | (int)stateToRender.animationState == 6)
            animController.SetFloat("RunSpeed", runSpeed);
    }

    // Move(args) method will move the player based on it's previous state and the keyboard input
    // then it updates all clients
    // It returns the PlayerState 
    PlayerState Move(PlayerState previous, KeyCode newKey)
    {
        float deltaX = 0, deltaY = 0, deltaZ = 0;
        float deltaRotationY = 0;
        bool running = false, jumping = false;

        if (newKey == KeyCode.Q)
        {
            
           deltaX = -0.5f;
           
        }
        else if (newKey == KeyCode.S)
        {
            if (newKey == KeyCode.LeftShift)
            {
                deltaZ = -0.5f;
                running = true;
            }
            else
            {
                deltaZ = -0.5f;
            }

        }
        else if (newKey == KeyCode.E)
        {
            deltaX = 0.5f;

        }
        else if (newKey == KeyCode.W)
        {
            if (newKey == KeyCode.LeftShift)
            {
                deltaZ = 1.0f;
                running = true;
            }
            else
            {
                deltaZ = 0.5f;
            }
        }
        else if (newKey == KeyCode.A)
        {
            deltaRotationY = -1f;

        }
        else if (newKey == KeyCode.D)
        {
            deltaRotationY = 1f;

        }
        else if (newKey == KeyCode.Space)       // If in put is Space, make the player Jump, but also implement gravity
        {
            if (this.IsGrounded)
            {
                deltaY = 2.0f;
                jumping = true;

                Debug.Log("I jumped up");

            }
            else
            {
                deltaY = deltaY + Physics.gravity.y * Time.deltaTime;
                jumping = false;
            }
        }

        return new PlayerState
        {
            movementNumber = 1 + previous.movementNumber,
            posX = deltaX + previous.posX,
            posY = deltaY + previous.posY,
            posZ = deltaZ + previous.posZ,
            rotX = previous.rotX,
            rotY = deltaRotationY + previous.rotY,
            rotZ = previous.rotZ,
            isRunning = running,
            isJumping = jumping,
            animationState = CalcAnimation(deltaX, deltaY, deltaZ, deltaRotationY, running, jumping)
        };   
    } // end Move()

    void OnServerStateChanged(PlayerState newState)
    {
        serverstate = newState;
        if (pendingMoves != null)
        {
            while (pendingMoves.Count > (predictedState.movementNumber - serverstate.movementNumber))
            {
                pendingMoves.Dequeue();
            }

            UpdatePredictedState();
        }
    }

    void UpdatePredictedState()
    {
        predictedState = serverstate;
        foreach (KeyCode movekey in pendingMoves)
        {
            predictedState = Move(predictedState, movekey);
        }
    }

    CharacterState CalcAnimation(float dX, float dY, float dZ, float dRotY, bool running, bool jumping)
    {
        if (dX == 0 && dY == 0 && dZ == 0)
            return CharacterState.Idle;

        if (jumping)
            return CharacterState.Jumping;

        if (dX != 0 || dZ != 0)
        {
            if (dX > 0 || dZ > 0)
            {
                if (running)
                    return CharacterState.RunningForward;
                else
                    return CharacterState.WalkingForward;
            }
            else if (dX < 0 || dZ < 0)
            {
                if (running)
                    return CharacterState.RunningBackwards;
                else
                    return CharacterState.WalkingBackwards;
            }

        }
        return CharacterState.Idle;
     

    }
}
